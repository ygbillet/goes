package goes

import (
	"time"

	"github.com/mgutz/logxi/v1"
)

// LoadEvents return all events stored in the eventstore.
func (s *SQLite) LoadEvents() ([]Event, error) {
	return s.getEvents("")
}

// LoadEventsByAggregate return events for an aggregate.
func (s *SQLite) LoadEventsByAggregate(aggregate string) ([]Event, error) {
	id := s.adaptToStorage(aggregate)

	return s.getEvents(
		"WHERE aggregate_id = ?",
		id,
	)
}

// LoadEventsByEventType return events filtered on type
func (s *SQLite) LoadEventsByEventType(eventType string) ([]Event, error) {
	etype := s.adaptToStorage(eventType)
	return s.getEvents(
		"WHERE type = ?",
		etype,
	)
}

// LoadEventsByEventType return events filtered on multiple type
func (s *SQLite) LoadEventsByEventTypes(eventTypes ...string) ([]Event, error) {
	pl, args := s.prepareStmtMultipleArgs(eventTypes)
	return s.getEvents(
		"WHERE type IN ("+pl+")",
		args...,
	)

}

// LoadEventsByEventType return events from date
func (s *SQLite) LoadEventsFromTimestamp(timestamp time.Time) ([]Event, error) {
	return s.getEvents(
		"WHERE timestamp >= ?",
		timestamp,
	)
}

// LoadEventsByEventType return events for an aggregate filtered from date
func (s *SQLite) LoadEventsByAggregateFromTimestamp(timestamp time.Time, aggregate string) ([]Event, error) {
	id := s.adaptToStorage(aggregate)

	return s.getEvents(
		"WHERE timestamp >= ? AND aggregate_id = ?",
		timestamp,
		id,
	)
}

// LoadEventsByEventType return events from date filtered by a single type
func (s *SQLite) LoadEventsByEventTypeFromTimestamp(timestamp time.Time, eventType string) ([]Event, error) {
	etype := s.adaptToStorage(eventType)
	return s.getEvents(
		"WHERE timestamp >= ? AND type = ?",
		timestamp,
		etype,
	)
}

// LoadEventsByEventType return events from date filtered by types
func (s *SQLite) LoadEventsByEventTypesFromTimestamp(timestamp time.Time, eventTypes ...string) ([]Event, error) {
	pl, args := s.prepareStmtMultipleArgs(eventTypes)
	return s.getEvents(
		"WHERE type IN ("+pl+") AND timestamp > ?",
		append(args, timestamp)...,
	)
}

// LoadEventsByEventType return events for an aggregate filtered from date
func (s *SQLite) getEvents(filter string, args ...interface{}) ([]Event, error) {
	var events []Event
	q := "SELECT id, aggregate_id, type, version, payload, timestamp FROM events"
	if filter != "" {
		q = q + " " + filter
	}
	log.Debug("goes/sqlite: getEvents", "query", q, "args", args)
	rows, err := s.db.Query(q, args...)
	if err != nil {
		log.Error("goes/sqlite: Cannot query DB",
			"err", err,
			"query", q,
			"args", args,
		)
		return events, err
	}
	defer rows.Close()

	for rows.Next() {
		var e Event
		err := rows.Scan(
			&e.ID,
			&e.StreamID,
			&e.Type,
			&e.Version,
			&e.Payload,
			&e.Timestamp,
		)
		if err != nil {
			log.Error("goes/sqlite: Cannot load events", "err", err)
			return events, err
		}
		events = append(events, e)
	}

	return events, nil
}

// prepareStmtMultipleArgs create placeholder (?, ?, ?) for SQL statement and
// append args as []byte in an []interface{}
func (s *SQLite) prepareStmtMultipleArgs(data []string) (string, []interface{}) {
	var args = make([]interface{}, 0)
	placeholders := "?"

	for i := 0; i < len(data); i++ {
		if i > 0 {
			placeholders = placeholders + ", ?"
		}
		if s.storage == Binary {
			args = append(args, []byte(data[i]))
		} else {
			args = append(args, data[i])
		}

	}

	return placeholders, args
}

func (s *SQLite) adaptToStorage(arg string) interface{} {
	if s.storage == Binary {
		return []byte(arg)
	}

	return arg
}
