package goes

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/mgutz/logxi/v1"
)

func (s *SQLite) checkConcurrency(streamID string, expected int64) error {
	var v int64

	err := s.db.QueryRow(
		"SELECT MAX(version) FROM events WHERE aggregate_id = ?",
		s.adaptToStorage(streamID),
	).Scan(&v)
	switch {
	case err == sql.ErrNoRows:
		return errors.New("missing aggregate")
	case err != nil:
		// We probably get a casting error (string "<nil>" to int64)
		log.Debug("goes/sqlite: get version", "err", err.Error())
		v = 0
	}

	if (v + 1) != expected {
		log.Debug("goes: concurrency error",
			"stream version", v,
			"event version", expected,
		)
		return fmt.Errorf("concurrency error: stream version %d, expected %d", v, expected)
	}

	return nil
}

func (s *SQLite) AppendEvents(events ...Event) error {

	if events[0].StreamID == "" {
		return errors.New("goes: missing aggregate_id")
	}
	if err := s.checkConcurrency(events[0].StreamID, events[0].Version); err != nil {
		return errors.New("goes: " + err.Error())
	}

	tx, errTransact := s.db.Begin()
	if errTransact != nil {
		log.Error("goes/sqlite: begin statement", "err", errTransact.Error())
		return errors.New("goes: error inserting sql")
	}
	stmt, errPrepare := tx.Prepare("INSERT INTO events (aggregate_id, type, version, payload, timestamp) VALUES (?, ?, ?, ?, ?)")
	if errPrepare != nil {
		log.Error("goes/sqlite: prepare statement", "err", errPrepare.Error())
		return errors.New("goes: error inserting sql")
	}
	defer stmt.Close()

	for _, e := range events {
		_, errExec := stmt.Exec(s.adaptToStorage(e.StreamID),
			s.adaptToStorage(e.Type),
			e.Version,
			e.Payload,
			e.Timestamp,
		)
		if errExec != nil {
			log.Error("goes/sqlite: insert event", "err", errExec.Error())
			return errors.New("goes: error inserting sql")
		}

	}
	if err := tx.Commit(); err != nil {
		log.Error("goes/sqlite: commit error", "err", err.Error())
		return errors.New("goes: error inserting sql")
	}

	return nil
}

func (s *SQLite) AppendEvent(e Event) (int64, error) {
	if e.StreamID == "" {
		return 0, errors.New("goes: missing aggregate_id")
	}

	if err := s.checkConcurrency(e.StreamID, e.Version); err != nil {
		return 0, errors.New("goes: " + err.Error())
	}

	_, err := s.db.Exec("INSERT INTO events (aggregate_id, type, version, payload, timestamp) VALUES (?, ?, ?, ?, ?)",
		s.adaptToStorage(e.StreamID),
		s.adaptToStorage(e.Type),
		e.Version,
		e.Payload,
		e.Timestamp,
	)

	if err != nil {
		log.Fatal("goes/sqlite: insert event", "err", err.Error())
	}

	return e.Version, nil
}
