package goes

import (
	"testing"
	"time"

	"github.com/mgutz/logxi/v1"
	"github.com/stretchr/testify/assert"
)

var (
	esreaderString EventStore
	esreaderBlob   EventStore
)

func init() {
	esreaderString, _ = NewSQLiteEventStore(":memory:", String)
	esreaderBlob, _ = NewSQLiteEventStore(":memory:", Binary)
	events := []Event{
		Event{
			Type:      "CreatedEvent",
			Timestamp: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
			Version:   1,
			StreamID:  "1",
			Payload:   "Evenement de test",
		},
		Event{
			Type:      "CreatedEvent",
			Timestamp: time.Date(2010, time.November, 10, 23, 0, 0, 0, time.UTC),
			Version:   1,
			StreamID:  "2",
			Payload:   "Evenement de test",
		},
		Event{
			Type:      "AddedEvent",
			Timestamp: time.Date(2011, time.November, 10, 23, 0, 0, 0, time.UTC),
			Version:   2,
			StreamID:  "1",
			Payload:   "Evenement de test 2",
		},
	}

	for _, event := range events {
		_, err := esreaderString.AppendEvent(event)
		if err != nil {
			log.Fatal("Failed to init tests", "err", err)
		}
		_, err = esreaderBlob.AppendEvent(event)
		if err != nil {
			log.Fatal("Failed to init tests", "err", err)
		}
	}
}

func TestLoadEvents(t *testing.T) {
	events, err := esreaderString.LoadEvents()
	assert.NoError(t, err)
	assert.Len(t, events, 3)
	assert.Equal(t, "Evenement de test", events[0].Payload)
	assert.Equal(t, "Evenement de test", events[1].Payload)
	assert.Equal(t, "Evenement de test 2", events[2].Payload)

	events, err = esreaderBlob.LoadEvents()
	assert.NoError(t, err)
	assert.Len(t, events, 3)
	assert.Equal(t, "Evenement de test", events[0].Payload)
	assert.Equal(t, "Evenement de test", events[1].Payload)
	assert.Equal(t, "Evenement de test 2", events[2].Payload)
}

func TestLoadEventsByAggregate(t *testing.T) {
	events, err := esreaderString.LoadEventsByAggregate("2")
	assert.NoError(t, err)
	assert.Len(t, events, 1)

	events, err = esreaderBlob.LoadEventsByAggregate("2")
	assert.NoError(t, err)
	assert.Len(t, events, 1)
}

func TestLoadEventsByEventType(t *testing.T) {
	events, err := esreaderString.LoadEventsByEventType("CreatedEvent")
	assert.NoError(t, err)
	assert.Len(t, events, 2)

	events, err = esreaderBlob.LoadEventsByEventType("CreatedEvent")
	assert.NoError(t, err)
	assert.Len(t, events, 2)
}

func TestLoadEventsByEventTypes(t *testing.T) {
	events, err := esreaderString.LoadEventsByEventTypes("CreatedEvent", "AddedEvent")
	assert.NoError(t, err)
	assert.Len(t, events, 3)

	events, err = esreaderBlob.LoadEventsByEventTypes("CreatedEvent", "AddedEvent")
	assert.NoError(t, err)
	assert.Len(t, events, 3)
}

func TestLoadEventsByEventTypesNotFound(t *testing.T) {
	events, err := esreaderString.LoadEventsByEventTypes("NotFound", "NotFoundBis")
	assert.NoError(t, err)
	assert.Len(t, events, 0)

	events, err = esreaderBlob.LoadEventsByEventTypes("NotFound", "NotFoundBis")
	assert.NoError(t, err)
	assert.Len(t, events, 0)
}

func TestLoadEventsFromTimestamp(t *testing.T) {
	events, err := esreaderString.LoadEventsFromTimestamp(time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC))
	assert.NoError(t, err)
	assert.Len(t, events, 2)

	events, err = esreaderBlob.LoadEventsFromTimestamp(time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC))
	assert.NoError(t, err)
	assert.Len(t, events, 2)
}

func TestLoadEventsByAggregateFromTimestamp(t *testing.T) {
	events, err := esreaderString.LoadEventsByAggregateFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"1",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 1)

	events, err = esreaderBlob.LoadEventsByAggregateFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"1",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 1)
}

func TestLoadEventsByEventTypeFromTimestamp(t *testing.T) {
	events, err := esreaderString.LoadEventsByEventTypeFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"CreatedEvent",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 1)

	events, err = esreaderBlob.LoadEventsByEventTypeFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"CreatedEvent",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 1)
}

func TestLoadEventsByEventTypesFromTimestamp(t *testing.T) {
	events, err := esreaderString.LoadEventsByEventTypesFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"CreatedEvent", "AddedEvent",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 2)

	events, err = esreaderBlob.LoadEventsByEventTypesFromTimestamp(
		time.Date(2010, time.September, 10, 10, 0, 0, 0, time.UTC),
		"CreatedEvent", "AddedEvent",
	)
	assert.NoError(t, err)
	assert.Len(t, events, 2)
}
