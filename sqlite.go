// Package sqlite implements an EventStore using an SQLite database.
package goes

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"github.com/mgutz/logxi/v1"
)

type Storage int

const (
	Binary Storage = iota
	String
)

type SQLite struct {
	db      *sql.DB
	storage Storage
}

// NewSQLiteEventStore return an EventStore using SQLite DB from path.
// Path can be :memory: for creating an in-memory sqlite db.
// A convinient use of in-memory will be for testing purpose.
func NewSQLiteEventStore(path string, storage Storage) (*SQLite, error) {
	var sqlStmt string

	db, err := sql.Open("sqlite3", path)
	if err != nil {
		log.Error("goes/sqlite: Cannot open/create database")
		return &SQLite{}, err
	}

	if storage == Binary {
		sqlStmt = `
		CREATE TABLE IF NOT EXISTS events (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			aggregate_id BLOB NOT NULL,
			type BLOB NULL,
			version INTEGER NOT NULL,
			payload TEXT NOT NULL,
			timestamp TIMESTAMP NOT NULL
		);`
	} else if storage == String {
		sqlStmt = `
		CREATE TABLE IF NOT EXISTS events (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			aggregate_id TEXT NOT NULL,
			type TEXT NULL,
			version INTEGER NOT NULL,
			payload TEXT NOT NULL,
			timestamp TIMESTAMP NOT NULL
		);`
	}

	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Error(
			"goes/sqlite: Cannot create table",
			"err", err,
			"sqlStmt", sqlStmt,
		)
		return &SQLite{}, err
	}

	return &SQLite{
		db:      db,
		storage: storage,
	}, nil

}
