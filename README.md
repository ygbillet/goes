# goes : Go EventStore #

An opinionated embedded eventstore for Go programs.
It use sqlite as storage mechanism.

Future releases will propose more storage mechanisms (e.g.: json file-based, in-memory).

**API IS NOT STABLE**

## Ideas ##

- Snapshots
- File-Based storage mechanism

## Sources of inspiration ##

- https://github.com/vizidrix/eventstore
- http://blog.jonathanoliver.com/event-sourcing-persistence/
- https://hackage.haskell.org/package/cqrs-sqlite3
- https://cqrs.wordpress.com/documents/building-event-storage/

## Contributing ##

1. Open an issue
2. Make pull request