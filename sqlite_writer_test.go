package goes

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var eswriter EventStore

func init() {
	eswriter, _ = NewSQLiteEventStore(":memory:", String)
}

func TestAppendEvent(t *testing.T) {
	v, err := eswriter.AppendEvent(Event{
		Type:      "TestEvent",
		Timestamp: time.Now(),
		Version:   1,
		StreamID:  "1",
		Payload:   "Evenement de test",
	})

	assert.NoError(t, err)
	assert.Equal(t, int64(1), v)
}

func TestAppendEvents(t *testing.T) {
	err := eswriter.AppendEvents(
		Event{
			Type:      "TestEvent",
			Timestamp: time.Now(),
			Version:   1,
			StreamID:  "2",
			Payload:   "Evenement de test",
		},
		Event{
			Type:      "TestEvent",
			Timestamp: time.Now(),
			Version:   2,
			StreamID:  "2",
			Payload:   "Evenement de test",
		},
	)

	assert.NoError(t, err)
}

func TestAppendEventConcurrency(t *testing.T) {
	_, err := eswriter.AppendEvent(Event{
		Type:      "TestEvent",
		Timestamp: time.Now(),
		Version:   1,
		StreamID:  "1",
		Payload:   "Evenement de test",
	})

	assert.Error(t, err)
}
