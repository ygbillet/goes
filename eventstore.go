// Package Evenstore provide interfaces for creating an eventstore.

package goes

import "time"

// EvenStore is an interface implemented by an eventstore
type EventStore interface {
	EventStoreReader
	EventStoreWriter
}

// EventWriter is responsible for persisting Events to the EventStore
type EventStoreWriter interface {
	// AppendEvent add an event to a SQLite EventStore.
	// If succed to append event, it return the version of the event.
	AppendEvent(Event) (int64, error)
	AppendEvents(...Event) error
}

// EventStoreReader is responsible for serving Streams as queries against the EventStore
type EventStoreReader interface {
	// LoadEvents return
	LoadEvents() ([]Event, error)

	// LoadEventsByAggregate return events from corresponding aggregate. This
	// function is usually used for rebuilding state of the aggregate.
	LoadEventsByAggregate(aggregate string) ([]Event, error)

	// LoadEventsByEventType returns events filtered by a single type.
	LoadEventsByEventType(eventType string) ([]Event, error)

	// LoadEventsByEventTypes returns events filtered by muktiple type.
	LoadEventsByEventTypes(eventTypes ...string) ([]Event, error)

	// LoadEventsFromTimestamp returns all events from a specific timestamp.
	LoadEventsFromTimestamp(timestamp time.Time) ([]Event, error)

	// LoadEventsByAggregateFromTimestamp returns events from a specific
	// timestamp for a specific aggregate.
	LoadEventsByAggregateFromTimestamp(timestamp time.Time, aggregate string) ([]Event, error)

	// LoadEventsByEventTypeFromTimestamp returns events with a specific
	// eventType from a specific timestamp.
	// This function is usually used for rebuilding read model.
	LoadEventsByEventTypeFromTimestamp(timestamp time.Time, eventType string) ([]Event, error)

	// LoadEventsByEventTypesFromTimestamp returns events filtered eventType
	// starting at timestamp.
	// This function is usually used for rebuilding read model.
	LoadEventsByEventTypesFromTimestamp(timestamp time.Time, eventTypes ...string) ([]Event, error)
}

// Event describe an event for an Eventstore.
// Id is the unique ID for the event.
// Type define the type of event
// Version is the event sequence number and used for optimistic concurrency.
// AggregateId refer to event's aggregate
// Payload contain the domain event formated as a string (e.g. json)
type Event struct {
	ID        string
	Type      string
	Timestamp time.Time
	Version   int64
	StreamID  string
	Payload   string
}
